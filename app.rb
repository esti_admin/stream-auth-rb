require_relative './init'

class RedisPool
  def method_missing(*args)
    REDIS.with do |conn|
      conn.send(*args)
    end
  end
end

class Pumatra < Sinatra::Base

  def self.redis
    @@redis ||= RedisPool.new
  end
  def redis ; self.class.redis end

  IPSET_PREFIX  = 'ipset:'.freeze
  IPSET_TTL     = 3 * 60 * 60 # 3 hours

  def ipset_key(user_id)
    "#{IPSET_PREFIX}#{user_id}"
  end

  # Adds user ip to sorted set with rank as current timestamp
  def add_user_ip(user_id, ip)
    key = ipset_key(user_id)
    # Cleans up ipset (with low probability not to overload)
    clean_user_ip(user_id) if rand < 0.01
    redis.zadd(
      key,
      Time.now.to_i,
      ip
    )
    # Arbitrarily set expire time for ipset (selfcleanup)
    redis.expire(key, IPSET_TTL)
  end

  def user_ips(user_id, time_frame=TFRAME)
    redis.zrangebyscore(
      ipset_key(user_id),
      Time.now.to_i - time_frame,
      Time.now.to_i + time_frame
    )
  end

  # Counts unique user IPs for given time frame (last n seconds)
  def count_user_ip(user_id, time_frame=TFRAME)
    user_ips(user_id).count
  end

  # Removes outdated user IPs (outside given time frame)
  def clean_user_ip(user_id, time_frame=TFRAME)
    redis.zremrangebyrank(
      ipset_key(user_id),
      0,
      Time.now.to_i - time_frame
    )
  end

  # Matches GET "http://host:port/"
  get "/auth/:ip/:user_id" do |ip, user_id|
    # Get set of user IPs
    ips = user_ips(user_id)
    # Get current clients count
    clients_count = ips.count

    current_count = ips.select { |stored_ip| stored_ip == ip }.count

    if (clients_count < USER_LIMIT && current_count == 0) || current_count == 1
      # Store user and ip
      add_user_ip(user_id, ip)
      status 200
    else
      status 403
      body 'Forbidden'
    end
  end

  get "/_/info/:user_id" do |user_id|
    match = redis.scan(0, match: "*#{user_id}*")
    # TODO: wrap it to PORO
    ips   = match.last.map { |key| key.split(":").last }
    {
      user_id: user_id,
      count: match.count,
      ips: ips

    }.to_json
  end

  get "/info" do
    match = redis.scan(0, match: "*")
    keys = match.last.map { |key| key.split(":").last }
    keys.map do |user_id, keys|
      [user_id, count_user_ip(user_id)]
    end.to_h.to_json
  end
end

