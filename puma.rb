root = File.expand_path(File.dirname(__FILE__))

pidfile "#{root}/tmp/puma.pid"
rackup "#{root}/config.ru"
state_path "#{root}/tmp/puma.state"

workers Integer(ENV['WEB_CONCURRENCY'] || 4)
threads_count = Integer(ENV['MAX_THREADS'] || 10)
threads threads_count, threads_count

port        ENV['PORT']     || 9000
environment ENV['RACK_ENV'] || 'development'

Puma::Single.prepend(Module.new do
  def stop
    super
  rescue NoMethodError
  end
end)
