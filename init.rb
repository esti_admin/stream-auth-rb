require 'sinatra'
require "redis"
require 'connection_pool'

# TODO: wrap configuration into module/class
config_path = "#{File.dirname(__FILE__)}/config.yml"

CONFIG = if File.exists?(config_path)
  YAML.load_file(config_path)
else
  {}
end

# Assumes Redis is running locally
# TODO: add redis connection configuration to config
REDIS = ConnectionPool.new(size: 10) { Redis.new }

# limits setup
# TODO: should be moved to module/class
limits = CONFIG.fetch('limits', {})

# Connection limit for single user
USER_LIMIT  = limits.fetch('user', 5)
# Connection limit for IP
IP_LIMIT    = limits.fetch('ip', 10)
# Time frame for limit check in seconds
TFRAME      = limits.fetch('frame', 30)

